########
# $Header$
#
#  Image indexer - mkidx
#   Copyright (c) 2003,2011, Michal Belica (http://www.beli.sk/)
#   All rights reserved.
#   
#   Redistribution and use in source and binary forms, with or without
#   modification, are permitted provided that the following conditions are met:
#   
#    * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#   
#   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#   DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
#   FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
#   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#   SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#   CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#   OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
########

# Make your changes here:
PREFIX=$(HOME)
BINDIR=bin
LIBDIR=bin/lib-mkidx
# for gd-2.x.x (does resampling instead of just resize)
GD_VER=2
# for gd-1.x.x
# GD_VER=1

# The lines below here shouldn't be changed
MAJOR=1
MINOR=0
REV=0
VERSION=$(MAJOR).$(MINOR).$(REV)
CFLAGS=-O3 -DVERSION=\"$(VERSION)\" -DGD_VER=$(GD_VER)
LDLIBS=-lgd -lm

CC=cc
SED=sed
INSTALL=install

all: imgres mkidx

clean:
	rm -f imgres mkidx

imgres: imgres.c
	$(CC) $(CFLAGS) imgres.c $(LDLIBS) -o imgres
	strip imgres

test:
	echo $(PREFIX)

mkidx: mkidx.shx
	$(SED) -e 's#%%PREFIX%%#$(PREFIX)#;s#%%BINDIR%%#$(PREFIX)/$(BINDIR)#;s#%%LIBDIR%%#$(PREFIX)/$(LIBDIR)#;s#%%VERSION%%#$(VERSION)#' mkidx.shx > mkidx
	chmod a+x mkidx

install: all
	$(INSTALL) -D -v --mode=0755 mkidx $(PREFIX)/$(BINDIR)/mkidx
	$(INSTALL) -D -v --mode=0711 imgres $(PREFIX)/$(LIBDIR)/imgres
	$(INSTALL) -D -v --mode=0644 default.css $(PREFIX)/$(LIBDIR)/default.css
	$(INSTALL) -D -v --mode=0644 default.conf $(PREFIX)/$(LIBDIR)/default.conf
