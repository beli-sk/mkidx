/*
 *  imgres - image resizing program, part of mkidx package
 *   Copyright (c) 2003,2011,2016, Michal Belica (http://www.beli.sk/)
 *   All rights reserved. Licensed under The BSD 2-Clause License.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <gd.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#ifndef VERSION
#	error "VERSION not defined, please use the original Makefile"
#endif

#ifndef GD_VER
#	warning "libgd version not defined, 2.x.x assumed"
#	define GD_VER 2
#endif

#define SC_W w0
#define SC_H h0
#define SC_R (((double) w0) / ((double) h0))

int main( int ac, char *av[] ) {
	gdImagePtr im1, im2;
	int w0,h0, w,h, w2,h2;
	FILE *fp;
	double a,b;

	if( ac != 5 ) {
		fprintf( stderr, "imgres of mkidx-%s\n", VERSION );
		fprintf( stderr, "use: imgres <width> <height> <file_in> <file_out>\n" );
		exit(1);
	}
	SC_W = atoi( av[1] );
	SC_H = atoi( av[2] );

	if( (fp = fopen( av[3], "r" )) == NULL ) {
		fprintf( stderr, "cannot open image\n" );
		exit(1);
	}
	if( (im1 = gdImageCreateFromJpeg( fp )) == NULL ) {
		fprintf( stderr, "cannot read image\n" );
		exit(1);
	}
	fclose( fp );
	w = gdImageSX( im1 );
	h = gdImageSY( im1 );
	a = ((double) w) / ((double) h);
	b = SC_R;
	if( a >= b ) { /* by width */
		w2 = SC_W;
		h2 = lround( ((double) w2) / a );
	} else { /* by height */
		h2 = SC_H;
		w2 = lround( ((double) h2) * a );
	}
#if GD_VER >= 2
#	warning "Compiling for gd version 2.x.x or later"
	im2 = gdImageCreateTrueColor( w2, h2 );
	gdImageCopyResampled( im2, im1, 0, 0, 0, 0, w2, h2, w, h );
#else
#	warning "Compiling for gd version 1.x.x"
	im2 = gdImageCreate( w2, h2 );
	gdImageCopyResized( im2, im1, 0, 0, 0, 0, w2, h2, w, h );
#endif
	if( (fp = fopen( av[4], "w" )) == NULL ) {
		fprintf( stderr, "cannot open output\n" );
		exit(1);
	}
	gdImageJpeg( im2, fp, -1 );
	fclose( fp );

	gdImageDestroy( im1 );
	gdImageDestroy( im2 );
	return 0;
}
